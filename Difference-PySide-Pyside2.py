""" Différences entre PySide et PySide2
Section 2, session 13
Les différences entre PySide et PySide2 sont assez minimes mais il est important de tout de même les connaître.

La différence majeure se situe au niveau de l'import du module.

En effet, beaucoup de fonctions contenues dans le module QtGui de PySide ont été déplacées dans le module QtWidgets dans PySide2.

Ainsi, si vous utilisez PySide2, importez le module QtWidgets en plus de QtGui et QtCore :

from PySide2 import QtWidgets, QtGui, QtCore 

Comme mentionné plus haut, vous devrez donc utiliser QtWidgets la place de QtGui dans la formation.

Par exemple, pour créer un QPushButton, vous devrez utiliser :

QtWidgets.QPushButton('Cliquez moi') 

à la place de

QtGui.QPushButton('Cliquez moi') 

Il existe quelques exceptions, par exemple, les classes QColor et QBrush sont toujours contenues dans QtGui et n'ont pas été déplacées dans QtWidgets.

Je vous conseille donc de toujours utiliser QtWidgets en premier lieu et si Python vous retourne une erreur et vous dit qu'il ne trouve pas la classe que vous utilisez dans QtWidgets, utilisez QtGui à la place.

Si jamais vous éprouvez d'autres difficultés, n'hésitez pas à me demander de l'aide sur le cours ou sur le serveur Discord.

Bon cours ! """