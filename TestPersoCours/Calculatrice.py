from PySide2 import QtWidgets, QtGui, QtCore


class Calculatrice(QtWidgets.QWidget):
    def __init__(self):
        super(Calculatrice, self).__init__()

        self.setWindowTitle('Calculatrice')


        layout = QtWidgets.QGridLayout(self)

        le_1 = QtWidgets.QLineEdit()
        le_1.setPlaceholderText("Calcul")

        le_2 = QtWidgets.QLineEdit()
        le_2.setPlaceholderText("Result")

        btn_1 = QtWidgets.QPushButton("1")
        btn_2 = QtWidgets.QPushButton("2")
        btn_3 = QtWidgets.QPushButton("3")
        btn_4 = QtWidgets.QPushButton("4")
        btn_5 = QtWidgets.QPushButton("5")
        btn_6 = QtWidgets.QPushButton("6")
        btn_7 = QtWidgets.QPushButton("7")
        btn_8 = QtWidgets.QPushButton("8")
        btn_9 = QtWidgets.QPushButton("9")
        btn_0 = QtWidgets.QPushButton("0")
        btn_C = QtWidgets.QPushButton("C")
        btn_A = QtWidgets.QPushButton("+")
        btn_S = QtWidgets.QPushButton("-")
        btn_D = QtWidgets.QPushButton("/")
        btn_M = QtWidgets.QPushButton("x")
        btn_V = QtWidgets.QPushButton(".")
        btn_E = QtWidgets.QPushButton("=")


        layout.addWidget(le_1, 0, 0, 1, 4)
        layout.addWidget(le_2, 1, 0, 1, 4)
        layout.addWidget(btn_C, 2, 0, 1, 1)
        layout.addWidget(btn_D, 2, 3, 1, 1)
        layout.addWidget(btn_1, 3, 0, 1, 1)
        layout.addWidget(btn_2, 3, 1, 1, 1)
        layout.addWidget(btn_3, 3, 2, 1, 1)
        layout.addWidget(btn_M, 3, 3, 1, 1)
        layout.addWidget(btn_4, 4, 0, 1, 1)
        layout.addWidget(btn_5, 4, 1, 1, 1)
        layout.addWidget(btn_6, 4, 2, 1, 1)
        layout.addWidget(btn_S, 4, 3, 1, 1)
        layout.addWidget(btn_7, 5, 0, 1, 1)
        layout.addWidget(btn_8, 5, 1, 1, 1)
        layout.addWidget(btn_9, 5, 2, 1, 1)
        layout.addWidget(btn_A, 5, 3, 1, 1)
        layout.addWidget(btn_0, 6, 1, 1, 1)
        layout.addWidget(btn_V, 6, 2, 1, 1)
        layout.addWidget(btn_E, 6, 3, 1, 1)
        
        

app = QtWidgets.QApplication([])

fenetre = Calculatrice()

fenetre.show()
app.exec_()