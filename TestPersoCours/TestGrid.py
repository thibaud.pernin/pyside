from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('GridApp')
        self.resize(500,300)

        layout = QtWidgets.QGridLayout(self)

        le_1 = QtWidgets.QLineEdit()
        le_1.setPlaceholderText("Enter your text.")

        btn_1 = QtWidgets.QPushButton("Close")
        btn_2 = QtWidgets.QPushButton("Save")
        btn_3 = QtWidgets.QPushButton("Edit")

        layout.addWidget(le_1, 0, 0, 2, 3)
        layout.addWidget(btn_3, 1, 2, 1, 1)
        layout.addWidget(btn_1, 2, 0, 1, 1)
        layout.addWidget(btn_2, 2, 2, 1, 1)
        

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()