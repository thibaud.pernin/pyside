import os
from glob import glob


CUR_DIR = os.path.dirname(__file__)
DATA_Folder = os.path.join(CUR_DIR, 'data')


def creerNote(nomDeLaNote, contenu=""):
    cheminDelaNote = os.path.join(DATA_Folder, nomDeLaNote + '.txt')

    with open(cheminDelaNote, 'w') as f:
        f.write(contenu)
    
    if os.path.isfile(cheminDelaNote):
        print("La Note '{}' a bien ete creee".format(nomDeLaNote))

    
def supprimerNote(nomDeLaNote):
    cheminDelaNote = os.path.join(DATA_Folder, nomDeLaNote + '.txt')

    if os.path.isfile(cheminDelaNote):
        os.remove(cheminDelaNote)
        print("La Note '{}' a bien ete supprimee".format(nomDeLaNote))
    else:
        print("La Note '{}' n'existe pas".format(nomDeLaNote))


def recupererLesNotes():
    notes = glob(DATA_Folder + '/*.txt')
    notes = [os.path.splitext(os.path.split(n)[-1])[0] for n in notes]
    return notes


def recupererContenuNote(nomDeLaNote):
    cheminDelaNote = os.path.join(DATA_Folder, nomDeLaNote + '.txt')
    with open(cheminDelaNote, 'r') as f:
        contenuDeLaNote = f.read()
    
    return contenuDeLaNote