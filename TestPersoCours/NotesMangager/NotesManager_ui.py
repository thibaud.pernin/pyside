from PySide2 import QtCore, QtGui, QtWidgets
from ui.FenetrePrincipale import Ui_FenetrePrincipale
import NotesManager as nm
import os

class CreateurDeNote(QtWidgets.QWidget, Ui_FenetrePrincipale):
    def __init__(self):
        super(CreateurDeNote, self).__init__()

        self.setupUi(self)
        self.recupererNotes()
        self.setupConnection()
        self.show()

    def setupConnection(self):
        self.btn_creerNote.clicked.connect(self.creerNote)
        self.lw_listeDeNotes.itemClicked.connect(self.afficherLaNote)
        self.btn_mettreAJourNote.clicked.connect(self.mettreAJourLaNote)
        self.btn_supprinerNote.clicked.connect(self.supprimerNote)


    def creerNote(self):
        nomDeLaNote, ok = QtWidgets.QInputDialog.getText(self, "Creer une note", 'Entrez le nom de la note: ')
        if not ok:
            return

        nm.creerNote(nomDeLaNote)
        self.recupererNotes()

    def recupererNoteSelectionnee(self):
        notesSelectionnees = self.lw_listeDeNotes.selectedItems()
        if not notesSelectionnees:
            return
        nomDeLaNote = notesSelectionnees[-1].text()
        cheminDeLaNote = os.path.join(nm.DATA_Folder, nomDeLaNote + '.txt')
        return nomDeLaNote, cheminDeLaNote

    def afficherLaNote(self):
        nomDeLaNote, cheminDeLaNote = self.recupererNoteSelectionnee()
        contenu = nm.recupererContenuNote(nomDeLaNote)
        self.te_ContenuDeLaNote.setText(contenu)

    def mettreAJourLaNote(self):
        nomDeLaNote, cheminDeLaNote = self.recupererNoteSelectionnee()
        contenu = self.te_ContenuDeLaNote.toPlainText()
        nm.creerNote(nomDeLaNote, contenu)

    def supprimerNote(self):
        nomDeLaNote, cheminDeLaNote = self.recupererNoteSelectionnee()
        nm.supprimerNote(nomDeLaNote)
        self.recupererNotes()
        self.te_ContenuDeLaNote.setText("")

    def recupererNotes(self):
        self.lw_listeDeNotes.clear()
        notes = nm.recupererLesNotes()
        self.lw_listeDeNotes.addItems(notes)
    

app = QtWidgets.QApplication([])
nc = CreateurDeNote()
app.exec_()