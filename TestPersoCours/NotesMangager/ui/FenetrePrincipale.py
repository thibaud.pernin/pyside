# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'c:/Users/Admin/Desktop/Python/Projets/Pyside/pyside/TestPersoCours/NotesMangager/ui\FenetrePrincipale.ui',
# licensing of 'c:/Users/Admin/Desktop/Python/Projets/Pyside/pyside/TestPersoCours/NotesMangager/ui\FenetrePrincipale.ui' applies.
#
# Created: Fri May  3 22:52:48 2019
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_FenetrePrincipale(object):
    def setupUi(self, FenetrePrincipale):
        FenetrePrincipale.setObjectName("FenetrePrincipale")
        FenetrePrincipale.resize(650, 570)
        self.gridLayout = QtWidgets.QGridLayout(FenetrePrincipale)
        self.gridLayout.setObjectName("gridLayout")
        self.btn_creerNote = QtWidgets.QPushButton(FenetrePrincipale)
        self.btn_creerNote.setObjectName("btn_creerNote")
        self.gridLayout.addWidget(self.btn_creerNote, 0, 0, 1, 1)
        self.btn_supprinerNote = QtWidgets.QPushButton(FenetrePrincipale)
        self.btn_supprinerNote.setObjectName("btn_supprinerNote")
        self.gridLayout.addWidget(self.btn_supprinerNote, 0, 1, 1, 1)
        self.te_ContenuDeLaNote = QtWidgets.QTextEdit(FenetrePrincipale)
        self.te_ContenuDeLaNote.setObjectName("te_ContenuDeLaNote")
        self.gridLayout.addWidget(self.te_ContenuDeLaNote, 1, 2, 1, 1)
        self.btn_mettreAJourNote = QtWidgets.QPushButton(FenetrePrincipale)
        self.btn_mettreAJourNote.setObjectName("btn_mettreAJourNote")
        self.gridLayout.addWidget(self.btn_mettreAJourNote, 2, 2, 1, 1)
        self.lw_listeDeNotes = QtWidgets.QListWidget(FenetrePrincipale)
        self.lw_listeDeNotes.setObjectName("lw_listeDeNotes")
        self.gridLayout.addWidget(self.lw_listeDeNotes, 1, 0, 1, 2)

        self.retranslateUi(FenetrePrincipale)
        QtCore.QMetaObject.connectSlotsByName(FenetrePrincipale)

    def retranslateUi(self, FenetrePrincipale):
        FenetrePrincipale.setWindowTitle(QtWidgets.QApplication.translate("FenetrePrincipale", "Createur de notes", None, -1))
        self.btn_creerNote.setText(QtWidgets.QApplication.translate("FenetrePrincipale", "Creer une note", None, -1))
        self.btn_supprinerNote.setText(QtWidgets.QApplication.translate("FenetrePrincipale", "Supprimer un note", None, -1))
        self.btn_mettreAJourNote.setText(QtWidgets.QApplication.translate("FenetrePrincipale", "Mettre à jour", None, -1))

