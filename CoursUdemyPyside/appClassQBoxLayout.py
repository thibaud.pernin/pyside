from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('Test Box Layout')
        self.resize(500,500)

        layout = QtWidgets.QVBoxLayout(self)
        #pour une version Horizontale du layout:
        #layout = QtWidgets.QHBoxLayout(self)

        btn_1 = QtWidgets.QPushButton("Bouton 1 ",self)
        btn_2 = QtWidgets.QPushButton("Bouton 2 ",self)
        btn_3 = QtWidgets.QPushButton("Bouton 3 ",self)

        layout.addWidget(btn_1)
        layout.addWidget(btn_2)
        layout.addWidget(btn_3)



app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()