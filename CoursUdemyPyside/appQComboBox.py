from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('ComboBoxApp')

        layout = QtWidgets.QHBoxLayout(self)

        cbb_demo = QtWidgets.QComboBox()

        cbb_demo.addItems(["First Item", 'Second Item', "Third Item"])
        #définir l'item par défaut: Par exemple le troisième
        cbb_demo.setCurrentIndex(2)



        layout.addWidget(cbb_demo)

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()