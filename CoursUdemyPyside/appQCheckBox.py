from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('CheckBoxApp')

        layout = QtWidgets.QHBoxLayout(self)

        chk_demo = QtWidgets.QCheckBox("This is a checkbox")
        #activer la chexkbox par défaut:
        chk_demo.setCheckState(QtCore.Qt.Checked)



        layout.addWidget(chk_demo)

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()