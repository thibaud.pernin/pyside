from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('ProgressBar')

        layout = QtWidgets.QHBoxLayout(self)

        prg_demo = QtWidgets.QProgressBar()
        prg_demo.setRange(0,10)
        prg_demo.setValue(5)
        #masquer le texte de progression:
        prg_demo.setTextVisible(False)

        layout.addWidget(prg_demo)

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()