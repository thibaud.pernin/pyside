from PySide2 import QtWidgets, QtGui, QtCore
from functools import partial

class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('Signaux Simples')

        layout = QtWidgets.QHBoxLayout(self)

        btn = QtWidgets.QPushButton('Cliquez ici!')
        btn.clicked.connect(partial(self.cliquersurlebouton, '1er bouton'))

        btn2 = QtWidgets.QPushButton('Ou là!')
        btn2.clicked.connect(partial(self.cliquersurlebouton, '2eme bouton'))
        

        layout.addWidget(btn)
        layout.addWidget(btn2)

    def cliquersurlebouton(self, text):
        print(text)

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()