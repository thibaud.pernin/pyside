from PySide2 import QtWidgets, QtGui, QtCore

app = QtWidgets.QApplication([]) #QApplication est la structure meme de l'application, a n'appeler qu'une seule fois

fenetre = QtWidgets.QWidget() # QWidget est la fenetre elle même et peut etre dupliquer ou reproduite (pop-up de fermeture pas ex.)
fenetre.setWindowTitle('Mon App')

w = 800
h = 500

fenetre.resize(w,h)

fenetre.move(1920/2 - w/2,1080/2 - h/2)

fenetre.show()
app.exec_()