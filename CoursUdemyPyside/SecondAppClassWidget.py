from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('My Second App')

app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()