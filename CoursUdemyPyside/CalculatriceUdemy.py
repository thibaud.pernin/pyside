from PySide2 import QtWidgets, QtGui, QtCore
from functools import partial


class CalculatriceUdemy(QtWidgets.QWidget):
    def __init__(self):
        super(CalculatriceUdemy, self).__init__()

        self.setWindowTitle('Calculatrice')
        
        self.setupUi()
        self.setupConnections()

        self.show()

    def setupUi(self):
        self.layout = QtWidgets.QGridLayout(self)

        self.le_c = QtWidgets.QLineEdit()

        self.le_r = QtWidgets.QLineEdit('0')

        self.btn_1 = QtWidgets.QPushButton("1")
        self.btn_2 = QtWidgets.QPushButton("2")
        self.btn_3 = QtWidgets.QPushButton("3")
        self.btn_4 = QtWidgets.QPushButton("4")
        self.btn_5 = QtWidgets.QPushButton("5")
        self.btn_6 = QtWidgets.QPushButton("6")
        self.btn_7 = QtWidgets.QPushButton("7")
        self.btn_8 = QtWidgets.QPushButton("8")
        self.btn_9 = QtWidgets.QPushButton("9")
        self.btn_0 = QtWidgets.QPushButton("0")
        self.btn_C = QtWidgets.QPushButton("C")
        self.btn_A = QtWidgets.QPushButton("+")
        self.btn_S = QtWidgets.QPushButton("-")
        self.btn_D = QtWidgets.QPushButton("/")
        self.btn_M = QtWidgets.QPushButton("x")
        self.btn_V = QtWidgets.QPushButton(".")
        self.btn_E = QtWidgets.QPushButton("=")


        self.layout.addWidget(self.le_c, 0, 0, 1, 4)
        self.layout.addWidget(self.le_r, 1, 0, 1, 4)
        self.layout.addWidget(self.btn_C, 2, 0, 1, 1)
        self.layout.addWidget(self.btn_D, 2, 3, 1, 1)
        self.layout.addWidget(self.btn_7, 3, 0, 1, 1)
        self.layout.addWidget(self.btn_8, 3, 1, 1, 1)
        self.layout.addWidget(self.btn_9, 3, 2, 1, 1)
        self.layout.addWidget(self.btn_M, 3, 3, 1, 1)
        self.layout.addWidget(self.btn_4, 4, 0, 1, 1)
        self.layout.addWidget(self.btn_5, 4, 1, 1, 1)
        self.layout.addWidget(self.btn_6, 4, 2, 1, 1)
        self.layout.addWidget(self.btn_S, 4, 3, 1, 1)
        self.layout.addWidget(self.btn_1, 5, 0, 1, 1)
        self.layout.addWidget(self.btn_2, 5, 1, 1, 1)
        self.layout.addWidget(self.btn_3, 5, 2, 1, 1)
        self.layout.addWidget(self.btn_A, 5, 3, 1, 1)
        self.layout.addWidget(self.btn_0, 6, 1, 1, 1)
        self.layout.addWidget(self.btn_V, 6, 2, 1, 1)
        self.layout.addWidget(self.btn_E, 6, 3, 1, 1)
        
        self.btns_nombres = []     

        for i in range(self.layout.count()):
            widget = self.layout.itemAt(i).widget()
            if isinstance(widget, QtWidgets.QPushButton):
                widget.setFixedSize(64,64)
                if widget.text().isdigit():
                    self.btns_nombres.append(widget)

    def setupConnections(self):
        for btn in self.btns_nombres:
            btn.clicked.connect(partial(self.btnNombrePressed, str(btn.text())))
        
        self.btn_S.clicked.connect(partial(self.btnOperationPressed, str(self.btn_S.text())))
        self.btn_A.clicked.connect(partial(self.btnOperationPressed, str(self.btn_A.text())))
        self.btn_D.clicked.connect(partial(self.btnOperationPressed, str(self.btn_D.text())))
        self.btn_M.clicked.connect(partial(self.btnOperationPressed, str(self.btn_M.text())))

        self.btn_E.clicked.connect(self.calculOperation)
        self.btn_C.clicked.connect(self.supprimerResultat)

    def btnNombrePressed(self, bouton):
        resultat = str(self.le_r.text())

        if resultat == '0':
            self.le_r.setText(bouton)
        else:
            self.le_r.setText(resultat + bouton)

    def btnOperationPressed(self, operation):
        operationText = str(self.le_c.text())
        resultat = str(self.le_r.text())

        self.le_c.setText(operationText + resultat + operation)
        self.le_r.setText('0')

    def supprimerResultat(self):
        self.le_r.setText('0')
        self.le_c.setText('')

    def calculOperation(self):
        resultat = str(self.le_r.text())

        self.le_c.setText(self.le_c.text() + resultat)
        resultatOperation = eval(str(self.le_c.text()))

        self.le_r.setText(str(resultatOperation))
    
    


app = QtWidgets.QApplication([])

fenetre = CalculatriceUdemy()

fenetre.show()
app.exec_()