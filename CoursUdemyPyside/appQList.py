from PySide2 import QtWidgets, QtGui, QtCore


class FenetrePrincipale(QtWidgets.QWidget):
    def __init__(self):
        super(FenetrePrincipale, self).__init__()

        self.setWindowTitle('Qlist items')

        layout = QtWidgets.QHBoxLayout(self)

        lw_demo = QtWidgets.QListWidget(self)
        lw_demo.addItem("Premier choix")
        lw_demo.addItem("Deuxième choix")

        #module de choix multiple en passant par une liste avec addItems([]):
        #lw_demo.addItems(["choix 1", "choix 2", "choix 3"])

        layout.addWidget(lw_demo)



app = QtWidgets.QApplication([])

fenetre = FenetrePrincipale()

fenetre.show()
app.exec_()